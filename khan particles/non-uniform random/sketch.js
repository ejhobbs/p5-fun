class RandomWalker {
    constructor(s, x, y) {
        this.sketch = s
        this.x = x;
        this.y = y;
        console.log(s)
    }

    display() {
        this.sketch.noStroke();
        this.sketch.fill(100,50,100);
        this.sketch.circle(this.x, this.y, 10);
    }

    walk() {
        let val = () => this.sketch.randomGaussian(0,5);
        let dx = val()
        let dy = val()
        this.x += dx
        this.y += dy
    }
}

let instance = new p5((s) => {
    const width = s.windowHeight-20;
    const height = s.windowHeight-20;
    s.setup = () => {
        s.frameRate(60);
        s.createCanvas(height, width)
    };

    let w = new RandomWalker(s, width/2, width/2);
    s.draw = () => {
        w.display();
        w.walk();
    };
}, document.getElementById("p5"));