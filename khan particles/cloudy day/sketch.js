const width = (offset = 20) => document.getElementById("p5").offsetWidth - offset
const height = (offset = 20) => document.getElementById("p5").offsetHeight - offset

let instance = new p5((s) => {
    const reset = () => {
        let canvas = s.createCanvas(width(), height());
        canvas.parent("p5");
    }
    s.setup = () => {
        reset();
        s.frameRate(30);
    };
    let zoff = 0;
    s.draw = () => {
        for (const xoff of range(0, 100)) {
            for (const yoff of range(0, 100)) {
                const c = s.map(s.noise(xoff*0.01, yoff*0.01, zoff), 0, 1, 0, 256);
                s.set(xoff, yoff, c);
            }
        }
        zoff += 0.01;
        s.updatePixels();
    };
});

function* range(current, end, inc = 1) {
    for (let i = current; i < end; i+=inc) {
        yield i
    }
}