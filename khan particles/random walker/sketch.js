class RandomWalker {
    constructor(s, x, y) {
        this.sketch = s
        this.x = x;
        this.y = y;
    }

    display() {
        this.sketch.noStroke();
        this.sketch.fill(100,50,100);
        this.sketch.circle(this.x, this.y, 10);
    }

    walk() {
        let dx = Math.floor(Math.random()*7)-3
        let dy = Math.floor(Math.random()*7)-3
        this.x += dx
        this.y += dy
    }
}

let instance = new p5((s) => {
    s.setup = () => {
        s.frameRate(60);
        s.createCanvas(s.windowHeight, s.windowWidth)
    };

    let w = new RandomWalker(s, s.windowHeight/2, s.windowWidth/2);
    s.draw = () => {
        w.display();
        w.walk();
    };

}, document.getElementById("p5"));