class NoisyWalker {
    constructor(s, x, y) {
        this.s = s
        this.x = x;
        this.y = y;
        this.tx = 0;
        this.ty = 1000;
        console.log(s)
    }

    display() {
        this.s.strokeWeight(2)
        this.s.point(this.x, this.y);
    }

    _noisy_steps() {
        const rand = (i) => this.s.map(this.s.noise(i), 0, 1, -3, 3);
        return [rand(this.tx+=0.01), rand(this.ty+=0.01)]
    }

    walk() {
        const [dx, dy] = this._noisy_steps()
        this.x += dx
        this.y += dy
    }
}
const width = (offset = 20) => document.getElementById("p5").offsetWidth - offset
const height = (offset = 20) => document.getElementById("p5").offsetHeight - offset

let instance = new p5((s) => {
    let walker = new NoisyWalker(s, width()/2, height()/2)
    const reset = () => {
        let canvas = s.createCanvas(width(), height())
        canvas.parent("p5")
    }
    s.setup = () => {
        reset()
    };
    s.draw = () => {
        if (s.mouseIsPressed) {
            reset()
        }
        walker.display()
        walker.walk()
    };
});