class Splatter {
    constructor(sketch, x, y, density, color) {
        this.s = sketch
        this.x = x
        this.y = y
        this.density = density
        this.color = color
    }

    draw() {
        this.s.fill(this.color)
        for (const i of range(0, 50)) {
            const [x, y, size] = this._place()
            this.s.circle(x, y, size)
        }
    }

    _point_size(offset) {
        let calculated = 50*(1/offset)
        return calculated > 20 ? 20 : calculated
    }

    _place() {
        const x = normal_random(this.s, this.x, this.density);
        const y = normal_random(this.s, this.y, this.density);
        const x_offset = Math.abs(x-this.x);
        const y_offset = Math.abs(y-this.y);
        const size = this._point_size((x_offset+y_offset)/2)
        return [x, y , size]
    }

}

function* range(current, end) {
    yield current;
    if (current == end) return;
    yield* range(current+1, end)
}

function normal_random(s, mean, sd) {
        return s.randomGaussian(mean, sd)
    }

function random_color(s, seed) {
    let r = normal_random(s, seed, 50)
    return s.color(
        normal_random(s, seed, 50),
        normal_random(s, seed, 50),
        normal_random(s, seed, 50))
}

const width = () => document.getElementById("p5").offsetWidth
const height = () => document.getElementById("p5").offsetHeight
const brightness = () => document.getElementById("brightness").value
const density = () => document.getElementById("density").value
const spread = () => document.getElementById("spread").value


let instance = new p5((s) => {
    s.reset = () => {
        let canvas = s.createCanvas(width(), height())
        canvas.parent("p5")
        s.clear();
        s.noStroke();
        s.background("#001010")
    }
    s.setup = () => {
        s.frameRate(60);
        s.reset()
    };
    s.draw = () => {
        if (s.mouseIsPressed) {
            s.reset()
        }
        const x_pos = normal_random(s, width()/2, width()/spread())
        const y_pos = normal_random(s, height()/2, height()/spread())
        const color = random_color(s, parseInt(brightness()))
        new Splatter(s, x_pos, y_pos, density(), color).draw()
    };
});