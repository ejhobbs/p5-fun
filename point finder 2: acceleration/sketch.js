const _2 = 2*(Math.PI/180)
const _90 = Math.PI / 2;
const _180 = Math.PI;
const _270 = Math.PI * 1.5;
const _360 = Math.PI * 2;

class Vector {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    distance(v) {
        const x2 = Math.pow(Math.abs(this.x - v.x), 2)
        const y2 = Math.pow(Math.abs(this.y - v.y), 2)
        return Math.sqrt(x2+y2)
    }

    diff(v) {
        const x = this.x - v.x
        const y = this.y - v.y
        return new Vector(Math.abs(x), Math.abs(y))
    }
}

class Pointer {
    _size = 15;
    _acceleration = 0.01;
    _speed_multiplier = 9;

    _heading = 0;
    _target_heading = this._heading;
    _speed = this._acceleration;
    _previous_distance = Infinity;

    _cos = (angle) => this._speed * Math.cos(angle) * this._speed_multiplier
    _sin = (angle) => this._speed * Math.sin(angle) * this._speed_multiplier

    constructor(target, position, colour) {
        this.target = target
        this.position = position
        this._colour = colour
    }

    draw(sketch) {
        this._update_heading();
        this._update_position();
        this._update_speed();
        sketch.drawColour(this._colour)
        sketch.circle(this.position.x, this.position.y, this._size)
    }

    _update_speed() {
        const current_distance = this.position.distance(this.target)
        if (current_distance <= this._previous_distance){
            if (this._speed < 1) {
                this._speed += this._acceleration
            }
        } else {
            const new_speed = this._speed - this._acceleration
            if(new_speed >= 0) {
                this._speed = new_speed
            }
        }
        this._previous_distance = current_distance
    }

    _update_heading() {
        const relative_heading = (_360 - this._heading + this._target_heading) % _360
        const inverse = _360 - relative_heading
        const angle = _2/this._speed
        if (relative_heading < inverse) {
            this._heading = (this._heading + angle) % _360
        } else {
            this._heading = (this._heading - angle) % _360
        }
    }

    _update_position() {
        let x_pos = this.position.x
        let y_pos = this.position.y
        let x, y, target_heading;
        const delta = this.position.diff(this.target)
        if (x_pos < this.target.x) {
            if (y_pos < this.target.y) {
                target_heading = Math.atan(delta.y/delta.x)
                x = x_pos + this._cos(this._heading)
                y = y_pos + this._sin(this._heading)
            } else {
                target_heading = Math.atan(delta.x / delta.y) + _270;
                x = x_pos + this._sin(this._heading - _270);
                y = y_pos - this._cos(this._heading - _270);
            }
        } else {
            if (y_pos < this.target.y) {
                target_heading = Math.atan(delta.x / delta.y) + _90
                x = x_pos - this._sin(this._heading - _90)
                y = y_pos + this._cos(this._heading - _90)
            } else {
                target_heading = Math.atan(delta.y / delta.x) - _180
                x = x_pos - this._cos(this._heading + _180)
                y = y_pos - this._sin(this._heading + _180)
            }
        }
        this.position = new Vector(x,y)
        this._target_heading = target_heading
    }
}

function width() { return document.getElementById("p5").clientWidth }
function height() { return document.getElementById("p5").clientHeight }

const instance = new p5((s) => {

    const pointers = []
    for (let i = 0; i < 100; i++) {
        const colour = s.color(Math.random()*256, Math.random()*256, Math.random()*256)

        const pointer = new Pointer(
          new Vector(width()/2, height()/2),
          new Vector(Math.random()*width(), Math.random()*height()),
          colour)
        pointers.push(pointer)
    }

    const reset = () => {
        let canvas = s.createCanvas(width(), height());
        canvas.parent("p5");
        s.clear()
        s.background("#001010")
    }

    s.keyPressed = () => {
        if (s.key === " ") {
            reset()
        }
    }
    s.drawColour = (c) => {
        s.fill(c);
        s.stroke(c);
    }
    s.setup = () => { reset(); }
    s.draw = () => {
        s.clear()
        s.background("#000303")
        for (const p of pointers) {
            p.target = new Vector(s.mouseX, s.mouseY)
            p.draw(s)
        }
    };
});
