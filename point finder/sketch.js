const _90 = Math.PI / 2;
const _180 = Math.PI;
const _270 = Math.PI * 1.5;
const _360 = Math.PI * 2;

class Vector {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
}

class Pointer {
    _heading = Math.random()*_360;
    _speed = 3;
    _size = 15;
    _angle_delta = 0.05;

    constructor(target, position, colour) {
        this.target = target;
        this.position = position
        this._colour = colour;
    }

    draw(sketch) {
        this.position = this._next_position();
        sketch.drawColour(this._colour)
        const { x, y } = this._next_position(this._size/2)
        sketch.line(this.position.x, this.position.y, x, y);
        sketch.circle(this.position.x, this.position.y, this._size)
    }

    _next_position(step = this._speed) {
        const cos = (angle) => 2 * (step * Math.cos(angle))
        const sin = (angle) => 2 * (step * Math.sin(angle))

        let x_pos = this.position.x
        let y_pos = this.position.y
        const angle = this._heading

        const delta_x = Math.abs(this.target.x - x_pos);
        const delta_y = Math.abs(this.target.y - y_pos);

        let x, y, target_heading;
        if (x_pos < this.target.x) {
            if (y_pos < this.target.y) {
                target_heading = Math.atan(delta_y/delta_x)
                x = x_pos + cos(angle)
                y = y_pos + sin(angle)
            } else {
                target_heading = Math.atan(delta_x / delta_y) + _270;
                x = x_pos + sin(angle - _270);
                y = y_pos - cos(angle - _270);
            }
        } else {
            if (y_pos < this.target.y) {
                target_heading = Math.atan(delta_x / delta_y) + _90
                x = x_pos - sin(angle - _90)
                y = y_pos + cos(angle - _90)
            } else {
                target_heading = Math.atan(delta_y / delta_x) - _180
                x = x_pos - cos(angle + _180)
                y = y_pos - sin(angle + _180)
            }
        }

        const relative_heading = (_360 - this._heading + target_heading) % _360
        const inverse = _360 - relative_heading
        if (relative_heading < inverse) {
            this._heading = (this._heading + this._angle_delta) % _360
        } else {
            this._heading = (this._heading - this._angle_delta) % _360
        }
        return new Vector(x,y)
    }
}

function width() { return document.getElementById("p5").offsetWidth - 20 }
function height() { return document.getElementById("p5").offsetHeight - 20 }

let instance = new p5((s) => {
    const pointers = []
    for (let i = 0; i < 10; i++) {
        const colour = s.color(Math.random()*256, Math.random()*256, Math.random()*256)

        const pointer = new Pointer(
          new Vector(width()/2, height()/2),
          new Vector(Math.random()*width(), Math.random()*height()),
          colour)
        pointers.push(pointer)
    }
    const reset = () => {
        let canvas = s.createCanvas(width(), height());
        canvas.parent("p5");
    }
    s.drawColour = (c) => {
        s.fill(c);
        s.stroke(c);
    }
    s.setup = () => {
        reset();
    }
    s.draw = () => {
        s.clear()
        s.background("#001010")
        s.drawColour("purple")
        s.circle(s.mouseX, s.mouseY, 5)
        for (const p of pointers) {
            p.target = new Vector(s.mouseX, s.mouseY)
            p.draw(s)
        }
    };
});
