const width = () => document.getElementById("p5").offsetWidth
const height = () => document.getElementById("p5").offsetHeight

let instance = new p5((s) => {
    const reset = () => {
        let canvas = s.createCanvas(width(), height())
        canvas.parent("p5")
    }
    s.setup = () => {
        s.frameRate(30);
        reset()
    };
    s.draw = () => {};
});