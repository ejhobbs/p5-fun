class Ball {
    constructor(s, colour, [x, y], [x_vel, y_vel], [x_bound, y_bound], size = 20) {
        this.x_pos = x;
        this.y_pos = y;
        this.x_vel = x_vel;
        this.y_vel = y_vel;
        this.x_bound = x_bound;
        this.y_bound = y_bound;
        this.size = size;
        this.colour = colour;
        this.p5 = s;
    }

    update() {
        this.x_pos += this.x_vel;
        this.y_pos += this.y_vel;
        const { x, y } = this.edges();
        if (x <= 0 || x >= this.x_bound) this.x_vel *= -1;
        if (y <= 0 || y >= this.y_bound) this.y_vel *= -1;
    }

    edges() {
        const x = this.x_pos + (this.size * Math.sign(this.x_vel));
        const y = this.y_pos + (this.size * Math.sign(this.y_vel));
        return { x, y };
    }

    draw() {
        this.p5.fill(this.colour);
        this.p5.circle(this.x_pos, this.y_pos, this.size * 2);
    }
}


function get_int(limit) {
    return Math.floor(Math.random() * limit)+1;
}

let instance = new p5((s) => {
    let boids = [];

    s.setup = () => {
        let box_width = s.windowWidth - 10;
        let box_height = s.windowHeight - 10;
        let count = 4000;
        let bounds = [box_width, box_height];
        while (count > 0) {
            let velocity = [get_int(20), get_int(20)];
            fill = s.color(get_int(255), get_int(255), get_int(255));
            size = get_int(30)
            boids.push(new Ball(s, fill, [count, count], velocity, bounds, size));
            count -= 1;
        }
        s.createCanvas(box_width, box_height);
        s.frameRate(40);
    };

    s.draw = () => {
        s.clear();
        s.background("#000404");
        boids.forEach(b => {
            b.draw();
            b.update();
        });
    };

}, document.getElementById("p5"));